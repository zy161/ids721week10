# IDS721 Week 10

## Setup Rust LLM Model

1. Use `cargo lambda new ids721-week10` to create a new lambda function
2. Add required dependencies for this project in `Cargo.toml`
```toml
[dependencies]
lambda_http = "0.11.1"
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
rand = "0.8.5"
openssl = { version = "0.10.35", features = ["vendored"] }

tokio = { version = "1", features = ["macros"] }

```
3. Implement functions in `main.rs` for using `rustformers/pythia-ggml`
4. Use `cargo lambda watch` to test locally, default input and response result are shown below

- <img src="img/running.png" style="width:600px;">
5. Use customized input to test the functionality
- <img src="img/customizedrunning.png" style="width:600px;">

## Containerize and Push to AWS ECR
1. Create `Dockerfile` in the project's root directory
```Dockerfile
# Stage 1: Build the application using a builder image that includes Rust and cargo-lambda
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

WORKDIR /usr/src/myapp
COPY . .

RUN cargo lambda build --release

# Stage 2: Use a minimal runtime image from AWS Lambda base images
FROM public.ecr.aws/lambda/provided:al2

WORKDIR /week10

# Copy the built executable and any other necessary files from the builder image
COPY --from=builder /usr/src/myapp/target/ ./
COPY --from=builder /usr/src/myapp/src/pythia-1b-q4_0-ggjt.bin ./

# Set the handler name in CMD as your function handler
ENTRYPOINT ["/week10/lambda/ids721-week10/bootstrap"]
```
2. After dockerize the project, create a registry on AWS ECR, follow the push commands to push the image

- <img src="img/ecr.png" style="width:600px;">

## Create Lambda Function for Use
1. Go to AWS Lambda page, create a new lambda function with container image
2. Customize the function, create a new functional URL and make it publicly accessible
- <img src="img/lambda.png" style="width:600px;">
3. Open the link, check if it's working with default input and response
- <img src="img/default.png" style="width:600px;">
4. Send query to the endpoint for testing
- <img src="img/test.png" style="width:600px;">  