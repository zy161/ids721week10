# Stage 1: Build the application using a builder image that includes Rust and cargo-lambda
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

WORKDIR /usr/src/myapp
COPY . .

RUN cargo lambda build --release

# Stage 2: Use a minimal runtime image from AWS Lambda base images
FROM public.ecr.aws/lambda/provided:al2

WORKDIR /week10

# Copy the built executable and any other necessary files from the builder image
COPY --from=builder /usr/src/myapp/target/ ./
COPY --from=builder /usr/src/myapp/src/pythia-1b-q4_0-ggjt.bin ./

# Set the handler name in CMD as your function handler
ENTRYPOINT ["/week10/lambda/ids721-week10/bootstrap"]
